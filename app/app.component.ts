import { Component, OnInit} from '@angular/core';
import { Response} from '@angular/http';
import { HttpService} from './http.service';
import {ProjectInfo} from './projectinfo';
import {doc} from './doc'

@Component({
    selector: 'my-app',
    templateUrl: 'app/html/projects.html',
    //styleUrls: ['app/Semantic/components/table.css', 'app/Semantic/components/button.css','app/css/table.css'],
    styleUrls: ['app/Semantic/semantic.min.css','app/css/table.css'],
    providers: [HttpService]
})
export class AppComponent implements OnInit { 
    projects: ProjectInfo[]=[];
    selected_project: ProjectInfo;
    docs: doc[] = [];
    selected_doc: doc;
    
    constructor(private httpService: HttpService){}
    
   ngOnInit(){       
    this.httpService.getData().subscribe((data: Response) => this.projects=data.json());
    
   }  

   onSelect(project:ProjectInfo){
    this.selected_project = project;
    this.addPhone(project.value2);  
   }

   addPhone(str:string){
    this.httpService.getData2(str).subscribe((data: Response) => this.docs=data.json());
    }

    Doc_Click(edoc:doc)
    {
        this.selected_doc = edoc; 
    }

}

