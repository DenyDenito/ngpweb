import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
 
@Injectable()
export class HttpService{
 
    constructor(private http: Http){ }
     
    getData(){
        //return this.http.get("http://localhost:60192/api/Values?docid=2298351")
        return this.http.get("http://localhost:60192/api/Values")
        //return this.http.get("testS.json")
    }
    getData2(projectid:string){
        return this.http.get("http://localhost:60192/api/Values?project_id=" + projectid);
        //return this.http.get("testS.json")
    }
}